#!/bin/bash
password=P455w0rd


##----check if there's currently anything working.
countworking=$(mysql --login-path=local grabit -sN --<<<"select COUNT(id) FROM reddit where reddit.status = 'working'")
countnew=$(mysql --login-path=local grabit -sN --<<<"select COUNT(id) FROM reddit where reddit.status = 'new'")
echo COUNTWORKING: $countworking
echo COUNTNEW:  $countnew
if [ $countworking -eq 0 ]; then
    echo "No Jobs in progress.  Lets look for a new job"
else
   echo "i'm already working"
   exit 0;
fi

if [ $countnew -gt 0 ]; then
   echo "lets get to work."
   ##----seeing what the next new link is
   listid=$(mysql --login-path=local grabit -sN --<<<"SELECT id FROM reddit where reddit.status = 'new' LIMIT 1")
   link=$(mysql --login-path=local grabit -sN --<<<"SELECT link FROM reddit where reddit.id = '$listid'")
   foldername=$(mysql --login-path=local grabit -sN --<<<"SELECT foldername FROM reddit where reddit.id = '$listid'")
   #ACTION THE LINK
    mysql --login-path=local grabit -sN --<<<"UPDATE reddit SET reddit.status = 'working' WHERE reddit.id = '$listid'"
   mkdir /var/www/html/downloads/$foldername
   cd /var/www/html/downloads/$foldername
   /usr/local/bin/gallery-dl $link 

else
   echo "No work to do"
   exit 0;
fi


if [ $? -eq 0 ]; then
    echo "Operation Successful"
    mysql --login-path=local grabit -sN --<<<"UPDATE reddit SET reddit.status = 'success' WHERE reddit.id = '$listid'"
else
    echo "Operation Failed"
    mysql --login-path=local grabit -sN --<<<"UPDATE reddit SET reddit.status = 'fail' WHERE reddit.id = '$listid'"
fi

